package pl.islandworld;

import org.apache.commons.lang.StringUtils;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.MarkerAPI;
import org.dynmap.markers.MarkerSet;
import pl.islandworld.entity.SimpleIsland;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Gnacik
 *
 */
public class Dynmap
{
    private IslandWorld plugin;
    private Plugin dynmap;
    private DynmapAPI api;
    private MarkerAPI markerapi;
    private MarkerSet set;
    private Conf conf;

    private Map<String, AreaMarker> resareas = new HashMap<String, AreaMarker>();

    private class Conf
    {
        private int isPerTick;
        private int timeTick;
        private int updateTime;

        private int strokeWeight;

        private double strokeOpacity;
        private double fillOpacity;

        private int sc;
        private int fc;

        public Conf(IslandWorld plugin)
        {
            isPerTick = 10;
            timeTick = 1;
            updateTime = 3600;
            strokeWeight = 2;
            strokeOpacity = 0.8;
            fillOpacity = 0.3;

            String strokeColor = "#00FF00";
            String fillColor = "#00FF00";

            sc = 0x00FF00;
            fc = 0x00FF00;

            try
            {
                sc = Integer.parseInt(strokeColor.substring(1), 16);
                fc = Integer.parseInt(fillColor.substring(1), 16);
            }
            catch (NumberFormatException nfx)
            {
                plugin.dynDebug("Invalid dynmap regionstyle stroke/fill color definition!");
                plugin.dynDebug(nfx.getMessage());
            }
        }
    }

    public Dynmap(IslandWorld plug)
    {
        plugin = plug;
    }

    public void onEnable()
    {
        plugin.dynDebug("Dynmap: Initializing support");
        PluginManager pm = plugin.getServer().getPluginManager();
        dynmap = pm.getPlugin("dynmap");
        if(dynmap == null)
        {
            plugin.dynDebug("Dynmap: Cannot find dynmap!");
            return;
        }
        api = (DynmapAPI)dynmap;

        plugin.getServer().getPluginManager().registerEvents(new OurServerListener(), plugin);

        if(dynmap.isEnabled())
            activate();
    }

    public void onCreate(SimpleIsland island)
    {
        plugin.dynDebug("Dynmap: onCreate");

        handleRegion(island);
    }

    public void onDelete(SimpleIsland island)
    {
        plugin.dynDebug("Dynmap: onDelete");

        final String markerid = "IS_" + String.valueOf(island.getX()) + "_" + String.valueOf(island.getZ());

        final AreaMarker m = set.findAreaMarker(markerid);
        if (m != null)
        {
            m.deleteMarker();
            plugin.dynDebug("Dynmap: Removed ok " + markerid);
        }
        else
            plugin.dynDebug("Dynmap: Marker not found");
    }

    private class OurServerListener implements Listener
    {
        @EventHandler(priority= EventPriority.MONITOR)
        public void onPluginEnable(PluginEnableEvent event)
        {
            Plugin p = event.getPlugin();
            String name = p.getDescription().getName();
            if(name.equals("dynmap"))
            {
                if(dynmap.isEnabled())
                    activate();
            }
        }
    }

    private class UpdateJob implements Runnable
    {
        List<SimpleIsland> regionsToDo;
        private boolean make = false;

        public UpdateJob(boolean makeList)
        {
            make = makeList;
        }

        @Override
        public void run()
        {
            plugin.dynDebug("Dynmap: UpdateJob");

            if (make)
            {
                make = false;

                Map<String, SimpleIsland> regions = plugin.getIsleList();
                if (regions != null && !regions.isEmpty())
                    regionsToDo = new ArrayList<>(regions.values());

                if (regionsToDo != null)
                    plugin.dynDebug("Dynmap: Todo list size: " + regionsToDo.size());
                else
                    plugin.dynDebug("Dynmap: Todo list empty");
            }
            if (regionsToDo != null)
            {
                for (int i = 0; i < conf.isPerTick; i++)
                {
                    if (regionsToDo.isEmpty())
                    {
                        regionsToDo = null;
                        break;
                    }
                    SimpleIsland is = regionsToDo.remove(regionsToDo.size()-1);
                    handleRegion(is);
                }
                plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, this, conf.timeTick);
            }
            else
            {
                plugin.dynDebug("Dynmap: UpdateJob finished");

                plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new UpdateJob(true), 20 * conf.updateTime);
            }
        }
    }

    private String formatInfoWindow(SimpleIsland island, AreaMarker m)
    {
        String v = "<div class=\"regioninfo\"><table>"
                + "<tr><th colspan='2'>%regionname%</th></tr>"
                + "<tr><td>Wlasciciel:</td><td>%owner%</td></tr>"
                + "%members%"
                + "</table></div>";

        v = v.replace("%regionname%", m.getLabel());
        v = v.replace("%owner%", island.getOwner());

        final List<String> members = island.getMembers();
        if (members != null && !members.isEmpty())
        {
            v = v.replace("%members%", "<tr><td>Czlonkowie:</td><td>" + StringUtils.join(members.toArray(), ", ") + "</td></tr>");
        }
        else
            v = v.replace("%members%", "");

        return v;
    }

    private void handleRegion(SimpleIsland island)
    {
        double[] x = null;
        double[] z = null;

        final World world = IslandWorld.getIslandWorld();
        final String name = "[" + island.getHash() + "]";
        final String markerid = "IS_" + String.valueOf(island.getX()) + "_" + String.valueOf(island.getZ());

        // plugin.dynDebug("Dynmap: Handle Region " + name);

        final int rs = Config.REGION_SPACING;
        final int is = Config.ISLE_SIZE;

        x = new double[4];
        z = new double[4];

        x[0] = (island.getX() * is) + rs;
        z[0] = (island.getZ() * is) + rs;

        x[1] = (island.getX() * is) + is - rs;
        z[1] = (island.getZ() * is) + rs;

        x[2] = (island.getX() * is) + is - rs;
        z[2] = (island.getZ() * is) + is - rs;

        x[3] = (island.getX() * is) + rs;
        z[3] = (island.getZ() * is) + is - rs;

        // plugin.dynDebug("coords" + x[0]  + z[0] + "|" + x[1]  + z[1] + "|" + x[2]  + z[2] + "|" + x[3]  + z[3]);

        AreaMarker m = resareas.remove(markerid);
        if(m == null)
        {
            m = set.createAreaMarker(markerid, name, false, world.getName(), x, z, false);
            if(m == null)
                return;

            m.setFillStyle(conf.fillOpacity, conf.fc);
            m.setLineStyle(conf.strokeWeight, conf.strokeOpacity, conf.sc);
        }
        else
        {
            m.setCornerLocations(x, z);
            m.setLabel(name);
        }

        String desc = formatInfoWindow(island, m);
        m.setDescription(desc);
    }

    private void activate()
    {
        markerapi = api.getMarkerAPI();
        if(markerapi == null)
        {
            plugin.dynDebug("Dynmap: Error loading dynmap marker API!");
            return;
        }

        set = markerapi.getMarkerSet("islandworld.markerset");
        if(set == null)
            set = markerapi.createMarkerSet("islandworld.markerset", "Wyspy", null, false);
        else
            set.setMarkerSetLabel("Wyspy");

        if(set == null)
        {
            plugin.dynDebug("Dynmap: Error creating marker set");
            return;
        }

        conf = new Conf(plugin);

        plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new UpdateJob(true), 100);   /* First time is 5 seconds */
    }
}