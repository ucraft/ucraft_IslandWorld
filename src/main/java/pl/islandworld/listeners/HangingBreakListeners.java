package pl.islandworld.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import pl.islandworld.IslandWorld;

/**
 * @author Gnacik
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class HangingBreakListeners implements Listener {
    private final IslandWorld plugin;

    public HangingBreakListeners(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBreakHanging(HangingBreakByEntityEvent event) {
        if (event.isCancelled())
            return;

        if (event.getRemover() instanceof Player) {
            final Player player = (Player) event.getRemover();
            final Entity entity = event.getEntity();

            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island"))
                return;

            if (entity != null && !plugin.canBuildOnLocation(player, entity.getLocation()))
                event.setCancelled(true);
        }
    }
}