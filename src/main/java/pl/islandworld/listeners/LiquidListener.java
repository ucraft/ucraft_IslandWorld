package pl.islandworld.listeners;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;


/**
 * @author Gnacik
 */
@SuppressWarnings("unused")
public class LiquidListener implements Listener {
    private final IslandWorld plugin;

    public LiquidListener(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockFromTo(BlockFromToEvent event) {
        final Block blockf = event.getBlock();
        final Block blockt = event.getToBlock();

        if (blockf != null && blockt != null) {
            if (blockf.getWorld() == IslandWorld.getIslandWorld()) {
                final String coordHash = String.valueOf(blockf.getX() / Config.ISLE_SIZE) + "-" + String.valueOf(blockf.getZ() / Config.ISLE_SIZE);
                SimpleIsland is = plugin.getIslandAt(coordHash);
                if (is != null) {
                    if (plugin.isInsideIsland(blockf.getLocation(), is) && !plugin.isInsideIsland(blockt.getLocation(), is)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}