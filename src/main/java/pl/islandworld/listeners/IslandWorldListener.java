package pl.islandworld.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;

@SuppressWarnings("unused")
public class IslandWorldListener implements Listener {
    private final IslandWorld plugin;

    public IslandWorldListener(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        if (player != null) {
            SimpleIsland island = plugin.getPlayerIsland(player);
            if (island != null) {
                island.setOwnerLoginTime(System.currentTimeMillis());
                island.setOwner(player.getName());
                return;
            }

            SimpleIsland island2 = plugin.getHelpingIsland(player);
            if (island2 != null) {
                island2.setOwnerLoginTime(System.currentTimeMillis());
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.isCancelled())
            return;

        final Block b = event.getClickedBlock();
        final Player p = event.getPlayer();
        final Action a = event.getAction();

        if (a == null || b == null || p == null)
            return;

        if (p.getWorld() != IslandWorld.getIslandWorld())
            return;

        if (a == Action.RIGHT_CLICK_BLOCK) {
            if (b.getType() == Material.OBSIDIAN) {
                final ItemStack i = p.getItemInHand();

                if (i != null && i.getType() == Material.BUCKET) {
                    if (!plugin.canBuildOnLocation(p, b.getLocation()))
                        return;

                    boolean obsnear = false;

                    for (int x = -1; x <= 1; x++) {
                        for (int y = -1; y <= 1; y++) {
                            for (int z = -1; z <= 1; z++) {
                                if (x == 0 && y == 0 && z == 0)
                                    continue;

                                Block nb = IslandWorld.getIslandWorld().getBlockAt(b.getX() + x, b.getY() + y, b.getZ() + z);

                                if (nb != null && nb.getType() == Material.OBSIDIAN) {
                                    obsnear = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!obsnear) {
                        final int amount = i.getAmount();

                        if (amount == 1) {
                            p.setItemInHand(null);
                        } else {
                            i.setAmount(amount - 1);
                            p.setItemInHand(i);
                        }
                        b.setType(Material.AIR);
                        p.getInventory().addItem(new ItemStack(Material.LAVA_BUCKET));
                        p.updateInventory();
                    }
                }
            }
        }
    }
}
