package pl.islandworld.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import pl.islandworld.IslandWorld;

/**
 * @author Gnacik
 */
@SuppressWarnings("unused")
public class ArmorStandListeners implements Listener {
    private final IslandWorld plugin;

    public ArmorStandListeners(IslandWorld iw) {
        this.plugin = iw;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onArmorStand(PlayerInteractAtEntityEvent event) {
        if (event.isCancelled())
            return;

        final Player player = event.getPlayer();

        if (player != null) {
            if (player.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (player.hasPermission("islandworld.bypass.island") || player.isOp())
                return;

            final Entity entity = event.getRightClicked();

            if (entity != null && entity.getType() == EntityType.ARMOR_STAND) {
                if (!plugin.canBuildOnLocation(player, entity.getLocation())){
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.isCancelled())
            return;

        final Entity target = event.getEntity();

        Player attacker = null;
        if (event.getDamager() instanceof Player) {
            attacker = (Player) event.getDamager();
        } else if (event.getDamager() instanceof Projectile) {
            if (((Projectile) event.getDamager()).getShooter() instanceof Player)
                attacker = (Player) ((Projectile) event.getDamager()).getShooter();
        }

        if (target != null && attacker != null) {
            if (attacker.getWorld() != IslandWorld.getIslandWorld())
                return;
            if (attacker.hasPermission("islandworld.bypass.island"))
                return;

            if (target.getType() == EntityType.ARMOR_STAND) {
                if (!plugin.canBuildOnLocation(attacker, target.getLocation())){
                    event.setCancelled(true);
                }

            }
        }
    }
}