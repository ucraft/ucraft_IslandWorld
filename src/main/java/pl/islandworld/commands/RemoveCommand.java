package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class RemoveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        String plName = "";
        if(strings.length > 0){
            plName = strings[0];
        }
        Player player = (Player) commandSender;
        IslandWorld plugin = IslandWorld.getInstance();
        if (plName == null || plName.isEmpty()) {
            CorePlugin.send(player, "&7Poprawne uzycie: &3/" + s + " <nick>");
            return false;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cTylko wlasciciel wyspy moze usuwac osoby.");
            return true;
        }

        if (isle != null) {
            final String friendName = plName.toLowerCase();
            // Check
            if (isle.isMember(friendName)) {
                isle.removeMember(friendName);
                CorePlugin.send(player, "&7Gracz &3" + friendName + " &7zostal usuniety z party.");
                plugin.removeHelping(friendName);
                final Player friend = Bukkit.getPlayer(friendName);
                if (friend != null && plugin.isInsideIslandCuboid(friend, isle)) {
                    plugin.teleToSpawn(friend);
                    CorePlugin.send(friend, "&7Zostales usuniety z party przez &3" + player.getName() + "&7.");
                }
            } else
                CorePlugin.send(player, "&cGracz o nicku &3" + friendName + " &cnie jest w twoim party.");
        } else
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
        return false;
    }
}
