package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;

/**
 * Created by Artur on 07.09.2015.
 */
public class ZadaniaCommand extends Command {

    public ZadaniaCommand() {
        super(new String[]{"zadania"});
        super.setDescription("Lista zadan");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player) sender;
        String ff = "0";
        if(args.length != 0){
            ff = args[0];
        }
        if (player == null)
            return;

        int from = 0;
        if (IslandWorld.plugin.isDigit(ff))
            from = Integer.valueOf(ff);

        IslandWorld.plugin.showChallengeList(player, player.getName().toLowerCase(), from);
    }
}
