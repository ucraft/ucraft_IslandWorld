package pl.islandworld.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class CompleteCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        String idnum = "";
        if(strings.length > 0){
            idnum = strings[0];
        }
        if (player == null)
            return true;

        if (idnum.isEmpty()) {
            CorePlugin.send(player, "&7Poprawne uzycie: &3/" + s + " <numer zadania>");
            return true;
        }

        if(!IslandWorld.plugin.isDigit(idnum)){
            CorePlugin.send(player, "&cNiepoprawny numer zadania zadania.");
            return true;
        }

        IslandWorld.plugin.completeChallenge(player, Integer.valueOf(idnum));
        return false;
    }
}
