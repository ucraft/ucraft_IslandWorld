package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 03.01.17.
 */
public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        IslandWorld plugin = IslandWorld.getInstance();
        Player player = (Player) commandSender;
        if(plugin.getPlayerIsland(player) != null){
            CorePlugin.send(player, "&cJestes wlascicielem wyspy. Nie mozesz opuscic party, ale mozesz usunac wyspe.");
            return true;
        }

        SimpleIsland island = plugin.getHelpingIsland(player);
        if (island != null) {
            plugin.removeHelping(player.getName());

            island.removeMember(player.getName());

            if (plugin.isInsideIslandCuboid(player, island))
                plugin.teleToSpawn(player);

            CorePlugin.send(player, "&7Opusciles party.");

            final Player owner = Bukkit.getPlayer(island.getOwner());
            if (owner != null)
                CorePlugin.send(owner, "&7Gracz &3" + player.getName() + " &7opuscil party.");
        } else
            CorePlugin.send(player, "&cNie jestes dodany do zadnego party.");
        return false;
    }
}
