package pl.islandworld.commands;

import com.sk89q.minecraft.util.commands.Command;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class ExpellCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
        IslandWorld plugin= IslandWorld.getInstance();
        Player player = (Player) commandSender;
        String plName = "";
        if(strings.length > 0){
            plName = strings[0];
        }
        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cTylko wlasciciel wyspy moze wykonac ta komende.");
            return true;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);
        if (isle != null) {
            if (plName.isEmpty()) {
                int count = plugin.expellPlayers(player, isle);

                if (count > 0)
                    CorePlugin.send(player, "&7Wyrzuciles " + count  + "osob z wyspy.");
                else
                    CorePlugin.send(player, "&cNa wyspie nie ma zadnych graczy.");
            } else {
                Player pl = Bukkit.getPlayer(plName);
                if (pl != null) {
                    if (plugin.isInsideIslandCuboid(pl, isle)) {
                        if (pl.isOp() || pl.hasPermission("islandworld.bypass.expell")) {
                            return true;
                        }

                        CorePlugin.send(pl, "&cZostales wyrzucony z wyspy " + player.getName());
                        plugin.teleToSpawn(pl);

                        CorePlugin.send(player, "&7Wyrzuciles jedna osobe z wyspy.");
                    } else
                        CorePlugin.send(player, "&cTego gracza nie ma na twojej wyspie.");
                } else{
                    CorePlugin.send(player, "&cNa wyspie nie ma zadnych graczy.");
                }
            }
        } else
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
        return true;
    }
}
