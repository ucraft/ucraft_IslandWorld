package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by artur9010 on 04.01.17.
 */
public class JoinCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        IslandWorld plugin = IslandWorld.getInstance();
        Player player = (Player) commandSender;
        final HashMap<String, String> tmpMap = new HashMap<>();
        tmpMap.putAll(plugin.getPartyList());

        for (Map.Entry<String, String> x : tmpMap.entrySet()) {
            final String ow = x.getKey();
            final String vi = x.getValue();

            if (ow != null && vi != null && vi.equalsIgnoreCase(player.getName().toLowerCase())) {
                plugin.getPartyList().remove(ow);

                final SimpleIsland island = plugin.getPlayerIsland(ow);
                if (island != null) {
                    final Player visitor = Bukkit.getPlayer(vi);
                    if (visitor != null) {
                        island.addMember(visitor.getName().toLowerCase());
                        plugin.setIsHelping(visitor.getName(), island);

                        CorePlugin.send(visitor, "&7Dolaczyles do party.");
                        CorePlugin.send(Bukkit.getPlayer(ow), "&7Gracz &3" + visitor.getName() + " &7dolaczyl do party.");
                    }
                }
            }
        }
        return true;
    }
}
