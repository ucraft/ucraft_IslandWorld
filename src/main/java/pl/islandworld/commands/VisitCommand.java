package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.MyLocation;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 03.01.17.
 */
public class VisitCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        String plName = "";
        if(strings.length > 0){
            plName = strings[0];
        }
        IslandWorld plugin = IslandWorld.getInstance();
        if(!player.hasPermission("islandworld.visit")){
            CorePlugin.send(player, "&cTa komenda wymaga aktywnego konta VIP.");
            return true;
        }

        if(plName == null || plName.isEmpty()){
            CorePlugin.send(player, "&7Poprawne uzycie: &3/" + s + " <nick>");
            return true;
        }

        SimpleIsland isle = plugin.getPlayerIsland(plName);
        SimpleIsland isleh = plugin.getHelpingIsland(plName);

        if (isle == null && isleh != null)
            isle = isleh;

        if(isle != null){
            Player target = Bukkit.getPlayer(plName);
            if(isle.isLocked() && !player.hasPermission("islandworld.visit.bypass")){
                CorePlugin.send(player, "&cWlasciciel wyspy nie zyczy sobie odwiedzin.");
                return true;
            }
            if(target != null){
                if(targetIsSafePoint(IslandWorld.getIslandWorld(), isle)){
                    plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, isle.getLocation());
                    CorePlugin.send(player, "&7Przeteleportowano na wyspe gracza &3" + target.getName());
                }else{
                    CorePlugin.send(player, "&cNie mozna przeteleportowac. (popros wlasciciela wyspy o wpisanie /fixhome)");
                }
            }else{
                CorePlugin.send(player, "&cGracz jest offline.");
            }
        }else{
            CorePlugin.send(player, "&cTen gracz nie posiada wyspy.");
        }
        return true;
    }

    private boolean targetIsSafePoint(World world, SimpleIsland isle) {
        MyLocation l = isle.getLocation();

        Block above = world.getBlockAt(l.getBlockX(), l.getBlockY() + 1, l.getBlockZ());
        Block targe = world.getBlockAt(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        Block under1 = world.getBlockAt(l.getBlockX(), l.getBlockY() - 1, l.getBlockZ());

        boolean piston = false;
        pistoncheck:
        {
            for (int x = -3; x <= 3; x++) {
                for (int y = -3; y <= 3; y++) {
                    for (int z = -3; z <= 3; z++) {
                        final Block nb = world.getBlockAt(targe.getX() + x, targe.getY() + y, targe.getZ() + z);

                        if (nb != null) {
                            switch (nb.getType()) {
                                case TRAP_DOOR:
                                case PISTON_BASE:
                                case PISTON_EXTENSION:
                                case PISTON_MOVING_PIECE:
                                case PISTON_STICKY_BASE:
                                    piston = true;
                                    break pistoncheck;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return !piston && above.getType() == Material.AIR && targe.getType() == Material.AIR && under1.getType() != Material.AIR && under1.getType() != Material.LAVA && under1.getType() != Material.TRAP_DOOR;
    }
}
