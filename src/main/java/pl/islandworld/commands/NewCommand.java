package pl.islandworld.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.WeApi;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

import static pl.islandworld.IslandWorld.plugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class NewCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        IslandWorld plugin = IslandWorld.getInstance();
        Player player = (Player) commandSender;
        String schematicName = "normal";
        if (player == null)
            return true;

        if (plugin.haveIsland(player)){
            CorePlugin.send(player, "&cPosiadasz juz wyspe.");
            return true;
        }

        if (plugin.isHelping(player)){
            CorePlugin.send(player, "&cAktualnie jestes dodany do innej wyspy.");
            return true;
        }

        if(haveToWaitToCreateIsland(player)){
            CorePlugin.send(player, "&cMozesz utworzyc nowa wyspe raz na godzine.");
            return true;
        }

        if (plugin.getFreeList() != null && !plugin.getFreeList().isEmpty()) {
            SimpleIsland newIsland = plugin.getFreeList().iterator().next();

            if (newIsland != null) {
                if (WeApi.loadSchematic(plugin, schematicName, player) == null){
                    CorePlugin.send(player, "&cSchemat nie istnieje. Zglos do Administratora.");
                }

                // Create Island
                plugin.onCreate(newIsland, player, schematicName);
                // Send info
                CorePlugin.send(player, "&7Wyspa zostala utworzona!");
            }else{
                CorePlugin.send(player, "&cBrak wolnych wysp, zglos sie do administratora.");
            }
        }else{ //cos co sie pewnie nigdy nie zdarzy :c #sadthings
            CorePlugin.send(player, "&cBrak wolnych wysp, zglos sie do administratora.");
        }
        return false;
    }

    private boolean haveToWaitToCreateIsland(Player player)
    {
        if (plugin.lastCreatedIsland.containsKey(player.getName().toLowerCase()))
        {
            long time = plugin.lastCreatedIsland.get(player.getName().toLowerCase());
            long diff = (System.currentTimeMillis() - time) / 1000;

            if (diff <= 3600) //czas w sekundach
                return true;
        }
        return false;
    }
}
