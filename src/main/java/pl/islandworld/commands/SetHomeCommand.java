package pl.islandworld.commands;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

public class SetHomeCommand extends Command {

    public SetHomeCommand() {
        super(new String[]{"sethome", "ustawdom"});
        super.setDescription("Ustaw nowy dom.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        //Bukkit.getServer().dispatchCommand(sender, "is sethome");
        IslandWorld plugin = IslandWorld.getInstance();
        Player player = (Player) sender;
        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cTylko wlasciciel wyspy moze zmieniac polozenie domu.");
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(isle != null){
            if(plugin.isInsideOwnIsland(player)){
                Location loc = player.getLocation();
                if(plugin.isSafeToTeleport(loc)){
                    isle.setLocation(loc);
                    CorePlugin.send(player, "&7Dom zostal ustawiony!");
                }else{
                    CorePlugin.send(player, "&cNie mozesz ustawic domu w tym miejscu (niebezpieczne miejsce).");
                }
            }else{
                CorePlugin.send(player, "&cNie znajdujesz sie na wyspie.");
            }
        }else{
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
        }
    }
}
