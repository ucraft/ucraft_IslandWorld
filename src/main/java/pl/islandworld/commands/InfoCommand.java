package pl.islandworld.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 03.01.17.
 */
public class InfoCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        String other = "";
        if(strings.length > 0){
            other = strings[0];
        }
        Player player = (Player) commandSender;
        IslandWorld plugin = IslandWorld.getInstance();
        if (other.isEmpty()) {
            SimpleIsland isle = plugin.getPlayerIsland(player);
            if (isle == null) {
                isle = plugin.getHelpingIsland(player);
            }
            if (isle == null) {
                CorePlugin.send(player, "&cNie posiadasz wyspy.");
                return true;
            }
            plugin.showInfo(player, player.getName(), isle);
        } else {
            SimpleIsland isle = plugin.getPlayerIsland(other);
            if (isle == null) {
                isle = plugin.getHelpingIsland(player);
            }
            if (isle == null) {
                CorePlugin.send(player, "&cTaki gracz nie istnieje lub nie posiada wyspy.");
                return true;
            }
            plugin.showInfo(player, other, isle);
            return true;
        }
        return true;
    }
}
