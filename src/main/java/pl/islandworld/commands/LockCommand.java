package pl.islandworld.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

import static pl.islandworld.IslandWorld.plugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class LockCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        if (player == null){
            return true;
        }
        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cAby zablokowac dostep do wyspy musisz byc jej wlascicielem.");
            return true;
        }
        if(!plugin.haveIsland(player)){
            CorePlugin.send(player, "&cNie posiadasz wyspy. &7Mozesz utworzyc nowa komenda &3/nowa");
        }

        SimpleIsland island = plugin.getPlayerIsland(player);
        if(!island.isLocked()){
            island.setLocked(true);
            CorePlugin.send(player, "&7Dostep do wyspy przez osoby trzecie zostal zablokowany.");
        }else{
            island.setLocked(false);
            CorePlugin.send(player, "&7Dostep do wyspy przez osoby trzecie zostal odblokowany.");
        }
        return false;
    }
}
