package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.entity.SimpleIsland;
import pl.ucraft.core.CorePlugin;

import java.util.List;

import static pl.islandworld.IslandWorld.plugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class AddCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length == 0){
            CorePlugin.send(commandSender, "&7Poprawne uzycie: &3/" + s + " <nick>");
            return true;
        }
        String plName = strings[0];
        Player player = (Player) commandSender;
        if (plName == null || plName.isEmpty()) {
            CorePlugin.send(player, "&7Poprawne uzycie: &3/" + s + " <nick>");
            return true;
        }

        SimpleIsland isle = plugin.getPlayerIsland(player);

        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cTylko wlasciciel wyspy moze dodawac osoby.");
            return true;
        }

        if (isle != null) {
            final Player friend = Bukkit.getPlayer(plName);
            if (friend != null && friend.isOnline() && player.canSee(friend)) {
                if (plugin.haveIsland(friend)){
                    CorePlugin.send(player, "&cKolega posiada juz wyspe.");
                    return true;
                }
                if (plugin.isHelping(friend)) {
                    CorePlugin.send(player, "&cKolega jest juz dodany do jakiegos party.");
                    return true;
                }
                if (plugin.getPartyList().containsKey(player.getName().toLowerCase())) {
                    plugin.showError(player, plugin.getLoc("error-party-visitor-wait")); //todo
                    return true;
                }

                if (plugin.getPartyList().containsValue(friend.getName().toLowerCase())){
                    plugin.showError(player, plugin.getLoc("error-party-owner-wait").replaceAll("%name%", friend.getName())); //todo
                    return true;
                }

                // Check
                final int pLimit = player.hasPermission("islandworld.vip.party") ? 25 : 10;
                // Members
                final List<String> members = isle.getMembers();
                if (members != null) {
                    if (members.size() >= pLimit){
                        CorePlugin.send(player, "&cParty jest pelne. (max 10 osob, VIP 25)");
                        return true;
                    }


                    if (!members.contains(plName.toLowerCase())) {
                        plugin.makePartyRequest(player, friend);

                        CorePlugin.send(player, "&7Gracz &3" + friend.getName() + " &7zostal zaproszony do party.");
                        CorePlugin.send(friend, "&7Gracz &3" + player.getName() + " &7zaprosil Cie do party.");
                        CorePlugin.send(friend, "&7Aby zaakceptowac wpisz &3/join&7.");
                    } else{
                        CorePlugin.send(player, "&cTen gracz nalezy juz do tego party.");
                    }

                } else
                    plugin.showError(player, plugin.getLoc("error-wg-region")); //todo
            } else
                CorePlugin.send(player, "&cGracz jest offline.");
        } else
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
        return true;
    }
}
