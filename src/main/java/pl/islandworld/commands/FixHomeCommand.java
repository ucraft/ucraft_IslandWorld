package pl.islandworld.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

import static pl.islandworld.IslandWorld.plugin;

public class FixHomeCommand extends Command {

    public FixHomeCommand() {
        super(new String[]{"fixhome"});
        super.setDescription("Poprawia punkt w ktorym znajduje sie home.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player) sender;
        SimpleIsland isle = plugin.getPlayerIsland(player);

        if (isle != null) {
            if (plugin.findIslandSpawn(isle, null))
                CorePlugin.send(player, "&Gotowe. Teraz mozesz uzyc &3/home");
            else
                CorePlugin.send(player, "&cNie mozemy znalezc odpowiedniego miejsca na nowe /home. Napisz do Administratora o pomoc.");
        } else
            CorePlugin.send(player, "&cNie posiadasz wyspy lub nie jestes jej wlascicielem.");
    }
}
