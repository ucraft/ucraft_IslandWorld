package pl.islandworld.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.MyLocation;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

import static pl.islandworld.IslandWorld.plugin;

/**
 * Created by Artur on 07.09.2015.
 */
public class HomeCommand extends Command {

    public HomeCommand() {
        super(new String[]{"home", "dom", "h"});
        super.setDescription("Teleport na wyspe.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player)sender;
        SimpleIsland isle = plugin.getPlayerIsland(player);
        final SimpleIsland isleh = plugin.getHelpingIsland(player);
        if (isle == null && isleh != null)
            isle = isleh;

        if (isle != null) {
            MyLocation dest_loc = isle.getLocation();
            if (!plugin.isSafeToTeleport(dest_loc)){
                CorePlugin.send(player, "&cTeleportacja nie jest bezpieczna. Wpisz /fixhome");
                return;
            }

            plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, dest_loc);
            plugin.removeNearbyMobsOnIsland(dest_loc);
        } else
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
    }
}
