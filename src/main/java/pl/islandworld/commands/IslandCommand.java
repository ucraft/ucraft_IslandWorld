package pl.islandworld.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import pl.ucraft.core.CorePlugin;

/**
 * Created by artur9010 on 04.01.17.
 */
public class IslandCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        CorePlugin.sendHeader(commandSender, "Komendy SkyBlock");
        CorePlugin.sendCommandHelp(commandSender, "new", "Stworz nowa wyspe. (/nowa)");
        CorePlugin.sendCommandHelp(commandSender, "delete", "Usun wyspe. (/usun)");
        CorePlugin.sendCommandHelp(commandSender, "info", "Informacje na temat wyspy.");
        CorePlugin.sendCommandHelp(commandSender, "home", "Teleportacja na wyspe. (/dom)");
        CorePlugin.sendCommandHelp(commandSender, "sethome", "Zmiana home.");
        CorePlugin.sendCommandHelp(commandSender, "fixhome", "Przymusowa zmiana home.");
        CorePlugin.sendCommandHelp(commandSender, "expell", "Wyrzuca niechcianych gosci z wyspy.");
        CorePlugin.sendCommandHelp(commandSender, "add", "Dodaj gracza do wyspy. (/dodaj)");
        CorePlugin.sendCommandHelp(commandSender, "remove", "Usun gracza z wyspy. (/wyrzuc)");
        CorePlugin.sendCommandHelp(commandSender, "leave", "Opusc party. (/opusc)");
        CorePlugin.sendCommandHelp(commandSender, "visit", "Odwiedz wyspe innego gracza. (/odwiedz)");
        CorePlugin.sendCommandHelp(commandSender, "lock", "Zablokuj odwiedziny twojej wyspy.");
        CorePlugin.sendCommandHelp(commandSender, "zadania", "Spis dostepnych zadan.");
        CorePlugin.sendCommandHelp(commandSender, "ukoncz", "Ukoncz zadanie.");
        return false;
    }
}
