package pl.islandworld.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.islandworld.entity.SimpleIsland;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.ucraft.core.CorePlugin;

import static pl.islandworld.IslandWorld.plugin;

public class DeleteCommand extends Command {
    public DeleteCommand() {
        super(new String[]{"delete", "usun"});
        super.setDescription("Usun wyspe.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        //Bukkit.getServer().dispatchCommand(sender, "is delete");
        Player player = (Player) sender;
        if(plugin.isHelping(player)){
            CorePlugin.send(player, "&cAby usunac wyspe, musisz byc jej wlascicielem.");
            return;
        }
        if(!plugin.haveIsland(player)){
            CorePlugin.send(player, "&cNie posiadasz wyspy.");
            return;
        }

        if(plugin.isOnDeleteList(player.getName().toLowerCase())){
            SimpleIsland isle = plugin.getPlayerIsland(player);
            if(isle != null){
                CorePlugin.send(player, "&7Wyspa zostala usunieta.");
                Bukkit.getOnlinePlayers().stream().filter(pl -> pl != null && plugin.isInsideIsland(pl, isle)).forEach(plugin::teleToSpawn);
                plugin.removeFromDeleteList(player.getName().toLowerCase());
                plugin.onDelete(isle, player.getName().toLowerCase());
            }
        }else{
            plugin.addToDeleteList(player.getName().toLowerCase());
        }
    }
}
