package pl.islandworld;

import org.apache.commons.lang.StringUtils;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import pl.islandworld.commands.*;
import pl.islandworld.entity.Challenge;
import pl.islandworld.entity.MyLocation;
import pl.islandworld.entity.SimpleIsland;
import pl.islandworld.entity.SimpleIslandV6;
import pl.islandworld.listeners.*;
import pl.islandworld.managers.SchematicManager;
import pl.themolka.cmds.command.Commands;
import pl.ucraft.core.CorePlugin;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Gnacik & artur9010
 */
@SuppressWarnings({"SpellCheckingInspection"})
public class IslandWorld extends JavaPlugin {
    private static boolean REGEN_IN_PROGRESS = false;

    public static boolean LOADED = false;

    public static boolean purgeInProgress = false;
    public static boolean breakPurge = false;
    public static IslandWorld plugin = null;
    private static FileConfiguration customConfig = null;
    private static File customConfigFile = null;
    private Biome DEFAULT_BIOME = Biome.JUNGLE;
    private List<Challenge> challengeList;
    private HashMap<String, List<Integer>> pChaList;
    private List<SimpleIsland> freeList;
    private HashMap<String, SimpleIsland> isleList;
    private HashMap<String, SimpleIsland> helpList;
    private HashMap<String, String> visitList;
    private HashMap<String, String> partyList;
    private HashMap<String, Integer> createLimits = new HashMap<>();
    private HashMap<String, SimpleIsland> coordList;
    private HashMap<UUID, SimpleIsland> uuidList;
    public HashMap<String, Long> lastCreatedIsland = new HashMap<>();
    private List<String> deleteList;
    private ItemStack globalReward = null;
    private static World world = null;
    private YamlConfiguration clConf;

    private Dynmap dm;

    public static IslandWorld getInstance() {
        return plugin;
    }

    private static void saveYamlFile(YamlConfiguration yamlFile, String fileLocation) {
        if (yamlFile != null) {
            File dataFolder = plugin.getDataFolder();
            File file = new File(dataFolder, fileLocation);
            try {
                yamlFile.save(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected static YamlConfiguration loadYamlFile(String file) {
        File dataFolder = plugin.getDataFolder();
        File yamlFile = new File(dataFolder, file);

        YamlConfiguration config = null;
        if (yamlFile.exists()) {
            try {
                config = new YamlConfiguration();
                config.load(yamlFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            config = new YamlConfiguration();
            try {
                if (plugin.getResource(file) != null) {
                    plugin.saveResource(file, false);
                    config = new YamlConfiguration();
                    config.load(yamlFile);
                } else {
                    config.save(yamlFile);
                }
            } catch (Exception e) {
                plugin.getLogger().severe("Could not create the " + file + " file!");
            }
        }
        return config;
    }

    private static void copyFileUsingStream(File source, File dest) throws IOException, NullPointerException {
        OutputStream os = null;
        try (InputStream is = new FileInputStream(source)) {
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            if (os != null) {
                os.close();
            }
        }
    }

    public void showWarn(String msg) {
        getLogger().warning(msg);
    }

    @Override
    public void onEnable() {

        plugin = this;

        freeList = new ArrayList<>();
        deleteList = new ArrayList<>();
        challengeList = new ArrayList<>();

        isleList = new HashMap<>();
        pChaList = new HashMap<>();

        helpList = new HashMap<>();
        visitList = new HashMap<>();
        partyList = new HashMap<>();
        coordList = new HashMap<>();
        uuidList = new HashMap<>();

        //todo: dont use shitty molka's api :<
        getCommand("island").setExecutor(new IslandCommand());
        Commands.register(this, IslandDev.class);
        getCommand("new").setExecutor(new NewCommand());
        Commands.register(this, DeleteCommand.class);
        Commands.register(this, HomeCommand.class);
        Commands.register(this, SetHomeCommand.class);
        getCommand("add").setExecutor(new AddCommand());
        getCommand("remove").setExecutor(new RemoveCommand());
        Commands.register(this, FixHomeCommand.class);
        Commands.register(this, ZadaniaCommand.class);
        getCommand("complete").setExecutor(new CompleteCommand());
        getCommand("lock").setExecutor(new LockCommand());
        getCommand("info").setExecutor(new InfoCommand());
        getCommand("visit").setExecutor(new VisitCommand());
        getCommand("leave").setExecutor(new LeaveCommand());
        getCommand("expell").setExecutor(new ExpellCommand());
        getCommand("join").setExecutor(new JoinCommand());

        // Create default config
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            copy(getResource("config.yml"), configFile);
        }

        final Config cfg = new Config(this);
        cfg.setupDefaults();

        if (getIslandWorld() == null) {
            showWarn("World named IslandWorld doesn't exists!");
            return;
        }

        if (getServer().getPluginManager().getPlugin("WorldEdit") != null) {
            try {
                saveOurResource("schematics/normal.schematic");
            } catch (Exception e) {
                showWarn("Error saving resource: " + e.getMessage());
                return;
            }
        } else {
            showWarn("WorldEdit enabled in config, but not installed, disabling it.");
        }

        final int spx = getSpawnWorld().getSpawnLocation().getBlockX();
        final int spy = getSpawnWorld().getSpawnLocation().getBlockY();
        final int spz = getSpawnWorld().getSpawnLocation().getBlockZ();
        if (getSpawnWorld() == getIslandWorld() && spx >= 0 && spz >= 0) {
            showWarn("Warning! Spawn point (" + spx + ", " + spy + ", " + spz + ") in world '" + getSpawnWorld().getName() + "' is in area reserved for islands.");
        }

        if (Config.REGION_SPACING > (Config.ISLE_SIZE / 2)) {
            showWarn("Warning! Region spacing is higher than half of island size, defaulting it to 1.");
            Config.REGION_SPACING = 1;
        }

        saveOurConfig(configFile);

        if (!loadDatFiles()) {
            showWarn("There is problem with loading dat files.");
            return;
        }

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        final String dateAsString = simpleDateFormat.format(System.currentTimeMillis());

        final File from = new File(getDataFolder(), "islelist.dat");
        final File to = new File(getDataFolder(), "backup/islelist_" + dateAsString + ".dat");

        if (from.exists()) {
            to.getParentFile().mkdirs();
            try {
                copyFileUsingStream(from, to);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        loadXMLFiles();

        if (generateCacheLists()) {
            File f = new File(getDataFolder(), "freelist.dat");
            if (f.exists())
                f.delete();

            freeList.clear();
        }

        final int currCount = freeList.size() + isleList.size();
        final int maxCount = Config.MAX_COUNT * Config.MAX_COUNT;

        if (currCount < maxCount) {
            getLogger().info("Our list count (" + currCount + ") is smaller than max config (" + maxCount + "), scheduling re-generate");

            REGEN_IN_PROGRESS = true;

            Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Regenerate(), 5 * 20);
        }
        if (currCount > maxCount) {
            getLogger().info("Island count (" + currCount + ") higher than max count (" + maxCount + ")!");
        }

        getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
        getServer().getPluginManager().registerEvents(new UUIDListener(this), this);
        getServer().getPluginManager().registerEvents(new HangingBreakListeners(this), this);
        getServer().getPluginManager().registerEvents(new HorseInvListener(this), this);
        getServer().getPluginManager().registerEvents(new ArmorStandListeners(this), this);
        getServer().getPluginManager().registerEvents(new IslandWorldListener(this), this);
        getServer().getPluginManager().registerEvents(new IslandProtectionListeners(this), this);
        getServer().getPluginManager().registerEvents(new LiquidListener(this), this);

        // Schedule autosave
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new AutoSave(), 60 * 1200, 60 * 1200);

        getLogger().info("Loaded " + freeList.size() + " free islands.");
        getLogger().info("Loaded " + isleList.size() + " taken islands.");
        getLogger().info("Loaded " + challengeList.size() + " challenges.");
        getLogger().info("Loaded " + pChaList.size() + " players completed challenges.");
        getLogger().info("Loaded " + helpList.size() + " helping players.");

        if (Config.AUTO_PURGE > 0)
            this.purgeIslands(null, Config.AUTO_PURGE, 0);
        if (Config.CREATE_LIMIT > 0)
            loadCreateLimits();

        dm = new Dynmap(this);
        dm.onEnable();

        LOADED = true;
    }

    public Location getIslandCenter(SimpleIsland island){
        int x1 = island.getX() * Config.ISLE_SIZE;
        int z1 = island.getZ() * Config.ISLE_SIZE;
        int centerX = x1 + (Config.ISLE_SIZE/2);
        int centerZ = z1 + (Config.ISLE_SIZE/2);
        Location center = new Location(world, centerX, 0, centerZ);
        return center;
    }

    public double getDistanceFromIslandCenter(Player player, SimpleIsland island){
        return getIslandCenter(island).distance(player.getLocation());
    }

    public double getDistanceFromIslandCenterInPercent(Player player, SimpleIsland island){
        return 0;
    }

    private SchematicManager getSchematicManager() {
        return new pl.islandworld.managers.NewSchematicManager(this);
    }

    public void loadCreateLimits() {
        clConf = loadYamlFile("limitCfg.yml");

        createLimits.clear();

        if (clConf.getConfigurationSection("limits") != null) {
            for (String key : clConf.getConfigurationSection("limits").getKeys(false)) {
                int value = clConf.getConfigurationSection("limits.").getInt(key);
                if (value > 0)
                    createLimits.put(key, value);
            }
        }
    }

    public void saveCreateLimits() {
        for (Entry<String, Integer> xx : createLimits.entrySet())
            clConf.set("limits." + xx.getKey(), xx.getValue());

        saveYamlFile(clConf, "limitCfg.yml");
    }

    public void incCreateLimit(Player player) {
        if (Config.CREATE_LIMIT > 0) {
            final String plName = player.getName().toLowerCase();

            if (!(createLimits.containsKey(plName)))
                createLimits.put(plName, 0);

            int current = createLimits.get(plName);
            current++;

            createLimits.put(plName, current);
        }
    }

    public int getCreateLimit(String plName) {
        if (Config.CREATE_LIMIT > 0) {
            if (createLimits.containsKey(plName)) {
                return createLimits.get(plName);
            } else
                return -1;
        } else
            return -2;
    }

    public void resetCreateLimit(String plName) {
        createLimits.put(plName, 0);
    }

    private void loadXMLFiles() {
        Document doc = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setIgnoringComments(true);

        File file = new File(getDataFolder(), "challenges.xml");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            copy(getResource("challenges.xml"), file);
        }

        try {
            doc = factory.newDocumentBuilder().parse(file);
        } catch (Exception e) {
            getLogger().warning("Could not load challenges.xml " + e.toString());
        }

        for (Node n = doc != null ? doc.getFirstChild() : null; n != null; n = n.getNextSibling()) {
            if ("list".equalsIgnoreCase(n.getNodeName())) {
                for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling()) {
                    if ("globalreward".equalsIgnoreCase(d.getNodeName())) {
                        final int itemId = Integer.parseInt(d.getAttributes().getNamedItem("itemId").getNodeValue());
                        final int count = Integer.parseInt(d.getAttributes().getNamedItem("count").getNodeValue());
                        if (d.getAttributes().getNamedItem("damage") != null)
                            globalReward = new ItemStack(itemId, count, (short) Integer.parseInt(d.getAttributes().getNamedItem("damage")
                                    .getNodeValue()));
                        else
                            globalReward = new ItemStack(itemId, count);
                    } else if ("challenge".equalsIgnoreCase(d.getNodeName())) {
                        int cid = Integer.parseInt(d.getAttributes().getNamedItem("id").getNodeValue());
                        String descr = null;
                        ItemStack req = null;
                        ItemStack rew;
                        List<ItemStack> reqItems = new ArrayList<>();
                        List<ItemStack> rewItems = new ArrayList<>();
                        double rewEco = 0;

                        boolean take = false;
                        boolean rep = false;

                        if (d.getAttributes().getNamedItem("repeatable") != null)
                            rep = Boolean.parseBoolean(d.getAttributes().getNamedItem("repeatable").getNodeValue());

                        for (Node p = d.getFirstChild(); p != null; p = p.getNextSibling()) {
                            final String nodeName = p.getNodeName();

                            switch (nodeName) {
                                case "descr":
                                    descr = p.getAttributes().getNamedItem("val").getNodeValue();
                                    break;
                                case "required": {
                                    final int itemId = Integer.parseInt(p.getAttributes().getNamedItem("itemId").getNodeValue());
                                    final int count = Integer.parseInt(p.getAttributes().getNamedItem("count").getNodeValue());
                                    if (p.getAttributes().getNamedItem("take") != null)
                                        take = Boolean.parseBoolean(p.getAttributes().getNamedItem("take").getNodeValue());

                                    if (p.getAttributes().getNamedItem("damage") != null)
                                        req = new ItemStack(itemId, count, (short) Integer.parseInt(p.getAttributes().getNamedItem("damage").getNodeValue()));
                                    else
                                        req = new ItemStack(itemId, count);

                                    reqItems.add(req);
                                    break;
                                }
                                case "reward": {
                                    final int itemId = Integer.parseInt(p.getAttributes().getNamedItem("itemId").getNodeValue());
                                    final int count = Integer.parseInt(p.getAttributes().getNamedItem("count").getNodeValue());
                                    if (p.getAttributes().getNamedItem("damage") != null)
                                        rew = new ItemStack(itemId, count, (short) Integer.parseInt(p.getAttributes().getNamedItem("damage").getNodeValue()));
                                    else
                                        rew = new ItemStack(itemId, count);

                                    rewItems.add(rew);
                                    break;
                                }
                                case "ecoreward":{
                                    rewEco = Double.parseDouble(p.getAttributes().getNamedItem("count").getNodeValue());
                                    break;
                                }
                            }
                        }

                        assert descr != null;
                        if (cid > 0 && !descr.isEmpty() && req != null) {
                            if (getChallenge(cid) != null)
                                getLogger().warning("Duplicate challenge ID : " + cid);
                            else
                                challengeList.add(new Challenge(cid, descr, reqItems, take, rewItems, rewEco, rep));
                        }
                    }
                }
            }
        }
    }

    private boolean loadDatFiles() {
        boolean needSave = false;
        boolean isImport = false;

        try {
            File v6listFile = new File(getDataFolder(), "islelistV6.dat");
            if (v6listFile.exists()) {
                HashMap<String, SimpleIslandV6> v6isleList = new HashMap<>();
                try {
                    v6isleList = (HashMap<String, SimpleIslandV6>) SLAPI.load(getDataFolder() + "/islelistV6.dat");
                } catch (Exception e) {
                    getLogger().warning("Error: " + e.getMessage());
                }
                if (v6isleList != null) {
                    int c = 0;
                    for (Entry<String, SimpleIslandV6> old : v6isleList.entrySet()) {
                        final String ownerName = old.getKey();
                        final SimpleIslandV6 iso = old.getValue();

                        SimpleIsland island = new SimpleIsland(iso);

                        isleList.put(ownerName, island);
                        c++;
                    }
                    getLogger().info("Imported " + c + " free islands from old v6 file");
                }
                v6listFile.delete();

                isImport = true;
                needSave = true;
            }

            if (isleList.isEmpty() && new File(getDataFolder(), "islelist.dat").exists()) {
                try {
                    isleList = (HashMap<String, SimpleIsland>) SLAPI.load(getDataFolder() + "/islelist.dat");
                } catch (Exception e) {
                    getLogger().warning("Error: " + e.getMessage());
                }
            }


            File v6file = new File(getDataFolder(), "freelistV6.dat");
            File vxfile = new File(getDataFolder(), "freelist.dat");
            if (isImport) {
                if (v6file.exists())
                    v6file.delete();
                if (vxfile.exists())
                    vxfile.delete();
            } else {
                if (freeList.isEmpty() && new File(getDataFolder(), "freelist.dat").exists()) {
                    try {
                        freeList = (ArrayList<SimpleIsland>) SLAPI.load(getDataFolder() + "/freelist.dat");
                    } catch (Exception e) {
                        getLogger().warning("Error: " + e.getMessage());
                    }
                }
            }

            if (new File(getDataFolder(), "compChallenges.dat").exists()) try {
                pChaList = (HashMap<String, List<Integer>>) SLAPI.load(getDataFolder() + "/compChallenges.dat");
            } catch (Exception e) {
                getLogger().warning("Error: " + e.getMessage());
            }

            if (needSave) {
                saveDatFiles();
            }
        } catch (Exception ignored) {
        }

        return true;
    }

    @Override
    public void onDisable() {
        if (LOADED)
            try {
                saveDatFiles();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static World getIslandWorld() {
        if (world == null)
            world = Bukkit.getServer().getWorld("IslandWorld");

        return world;
    }

    public World getSpawnWorld() {
        return CorePlugin.getSpawnLocation().getWorld();
    }

    public ItemStack getGlobalReward() {
        return globalReward;
    }

    private void copy(InputStream in, File file) {
        if (in == null)
            getLogger().warning("Cannot copy, resource null");

        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            if (in != null) {
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
            out.close();
            if (in != null) {
                in.close();
            }
        } catch (Exception e) {
            getLogger().warning("Error copying resource: " + e);
            e.printStackTrace();
        }
    }

    public Challenge getChallenge(int id) {
        for (Challenge cha : getChallenges()) {
            if (cha != null && cha.getId() == id)
                return cha;
        }
        return null;
    }

    public List<Challenge> getChallenges() {
        return challengeList;
    }

    public List<SimpleIsland> getFreeList() {
        return freeList;
    }

    public HashMap<String, SimpleIsland> getIsleList() {
        return isleList;
    }

    public HashMap<String, SimpleIsland> getCoordList() {
        return coordList;
    }

    public HashMap<UUID, SimpleIsland> getUUIDList() {
        return uuidList;
    }

    public SimpleIsland getIslandAt(String coordHash) {
        if (coordList.containsKey(coordHash)) {
            return coordList.get(coordHash);
        }

        return null;
    }

    public boolean haveIsland(Player player) {
        return haveIsland(player.getName());
    }

    public boolean haveIsland(String playerName) {
        return playerName != null && getIsleList().containsKey(playerName.toLowerCase());
    }

    public SimpleIsland getPlayerIsland(Player player) {
        return getPlayerIsland(player.getName());
    }

    public SimpleIsland getPlayerIsland(String playername) {
        return getIsleList().get(playername.toLowerCase());
    }

    public HashMap<String, List<Integer>> getCompletedChallenges() {
        return pChaList;
    }

    public void deleteChallenges(Player player) {
        deleteChallenges(player.getName().toLowerCase());
    }

    public void deleteChallenges(String name) {
        pChaList.remove(name);
    }

    public boolean playerCompletedChallenge(String plName, int id) {
        return pChaList.containsKey(plName.toLowerCase()) && pChaList.get(plName.toLowerCase()).contains(id);
    }

    private boolean containsAtLeast(Player player, ItemStack item, int amount) {
        if (item == null) {
            return false;
        }
        if (amount <= 0) {
            return true;
        }
        for (ItemStack i : player.getInventory().getContents()) {
            if (item.isSimilar(i) && (amount -= i.getAmount()) <= 0) {
                return true;
            }
        }
        return false;
    }

    public boolean completeChallenge(Player player, int id) {
        if (id > 0) {
            final Challenge cha = getChallenge(id);
            if (cha != null) {
                if (playerCompletedChallenge(player.getName(), id) && !cha.isRepeatable()){
                    CorePlugin.send(player, "&cTo zadanie zostalo juz ukonczone.");
                    return true;
                }

                final ItemStack gRew = getGlobalReward();

                final List<ItemStack> reqI = cha.getRequiredItems();
                final List<ItemStack> rewI = cha.getRewardItems();
                final Double rewEco = cha.getRewEco();

                final boolean take = cha.takeRequiredItems();

                if (reqI != null && !reqI.isEmpty()) {
                    boolean haveAll = false;
                    for (ItemStack reqItem : reqI) {
                        if (reqItem != null) {
                            if (containsAtLeast(player, reqItem, reqItem.getAmount())) {
                                haveAll = true;
                            } else {
                                haveAll = false;
                                break;
                            }
                        }
                    }

                    if (haveAll) {
                        if (take) {
                            reqI.stream().filter(Objects::nonNull).forEach(reqItem -> {
                                player.getInventory().removeItem(reqItem);
                            });
                        }

                        CorePlugin.send(player, "&7Zadanie &3#" + id + " &7ukonczone.");

                        if (pChaList.containsKey(player.getName().toLowerCase())) {
                            pChaList.get(player.getName().toLowerCase()).add(id);
                        } else {
                            List<Integer> list = new ArrayList<>();
                            list.add(id);
                            pChaList.put(player.getName().toLowerCase(), list);
                        }

                        if (gRew != null) {
                            player.getInventory().addItem(gRew);
                            CorePlugin.send(player, "&7Za ukonczenie zadania otrzymujesz: &3" + gRew.getAmount() + " " + gRew.getType());
                        }

                        if(rewEco != 0){
                            CorePlugin.economy.depositPlayer(player, rewEco);
                            CorePlugin.send(player, "&7Za ukonczenie zadania otrzymujesz: &3" + rewEco + "$&7.");
                        }

                        if (rewI != null && !rewI.isEmpty()) {
                            for (ItemStack rewItem : rewI) {
                                player.getInventory().addItem(rewItem);
                                CorePlugin.send(player, "&7Za ukonczenie zadania otrzymujesz: &3" + rewItem.getAmount() + " " + rewItem.getType());
                            }
                        }
                        player.updateInventory();
                    } else{
                        CorePlugin.send(player, "&cNie posiadasz wymaganych przedmiotow.");
                        return true;
                    }
                } else
                    return showError(player, getLoc("info-cha-no-req-item")); //todo
            } else{
                CorePlugin.send(player, "&cWystapil problem z zadaniem. :<");
                return true;
            }
        } else{
            CorePlugin.send(player, "&cNiepoprawne ID zadania.");
            return true;
        }

        return true;
    }

    public void saveDatFiles() throws Exception {
        SLAPI.save(isleList, getDataFolder() + "/islelist.dat");
        SLAPI.save(freeList, getDataFolder() + "/freelist.dat");
        SLAPI.save(pChaList, getDataFolder() + "/compChallenges.dat");

        if (Config.CREATE_LIMIT > 0)
            saveCreateLimits();
    }

    public void saveBackupFile(CommandSender sender) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        final String dateAsString = simpleDateFormat.format(System.currentTimeMillis());

        sender.sendMessage("Saving backup file...");
        boolean saved = true;
        try {
            SLAPI.save(isleList, getDataFolder() + "/backup/islelist_" + dateAsString + ".dat");
        } catch (Exception e) {
            saved = false;
            sender.sendMessage("Error saving backup " + e);
        }
        if (saved)
            sender.sendMessage("Backup saved");
    }

    public boolean showError(CommandSender sender, String text) {
        if (sender != null) {
            if (sender instanceof Player)
                sender.sendMessage(ChatColor.RED + text);
            else
                sender.sendMessage(text);
        }

        return false;
    }

    public boolean showMessage(CommandSender sender, String text) {
        if (sender != null)
            sender.sendMessage(ChatColor.GREEN + text);

        return false;
    }

    public boolean showOpMessage(CommandSender sender, String text) {
        if (sender != null)
            sender.sendMessage(ChatColor.GRAY + text);

        return false;
    }

    public void scheduleRebuild(SimpleIsland isle, Player player, String schema) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Rebuild(isle, player, schema));
    }

    private String hashMe(int x, int z) {
        return String.valueOf(x) + "-" + String.valueOf(z);
    }

    private void clearEntites(SimpleIsland island) {
        int x = (island.getX() * Config.ISLE_SIZE); // +1;
        int z = (island.getZ() * Config.ISLE_SIZE); // +1;
        int y = 0;

        int xx = x + Config.ISLE_SIZE; // -2;
        int yy = 256;
        int zz = z + Config.ISLE_SIZE; // -2;

        for (Entity i : world.getEntities()) {
            if (i != null) {
                final Location il = i.getLocation();
                if (il.getX() >= x && il.getX() <= xx && il.getY() >= y && il.getY() <= yy && il.getZ() >= z && il.getZ() <= zz) {
                    i.remove();
                }
            }
        }
    }

    public boolean findIslandSpawn(SimpleIsland island, CommandSender sender) {
        final int rs = island.getRegionSpacing();
        // Make math
        int x1 = (island.getX() * Config.ISLE_SIZE) + rs;
        int z1 = (island.getZ() * Config.ISLE_SIZE) + rs;

        int x2 = ((island.getX() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;
        int z2 = ((island.getZ() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;

        for (int y_operate = 250; y_operate > 0; y_operate--) {
            for (int x_operate = x1; x_operate < x2; x_operate++) {
                for (int z_operate = z1; z_operate < z2; z_operate++) {
                    Block b1 = world.getBlockAt(x_operate, y_operate, z_operate);
                    Block b2 = world.getBlockAt(x_operate, y_operate + 1, z_operate);
                    Block b3 = world.getBlockAt(x_operate, y_operate + 2, z_operate);

                    final Material b1t = b1.getType();

                    if (b2.getType() == Material.AIR && b3.getType() == Material.AIR && b1t.isSolid() && b1t != Material.CACTUS) {
                        final Location l = b2.getLocation();
                        l.setX(l.getBlockX() + 0.5);
                        l.setZ(l.getBlockZ() + 0.5);

                        island.setLocation(l);

                        final String msb = "Spawn changed to : " + b2.getLocation().toString();
                        if (sender != null)
                            sender.sendMessage(msb);

                        return true;
                    }
                }
            }
        }

        Location nloc = new Location(world, island.getX() * Config.ISLE_SIZE, Config.ISLE_HEIGHT, island.getZ() * Config.ISLE_SIZE);
        island.setLocation(nloc);
        return false;
    }

    private void deleteLayers(SimpleIsland playerIsland) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 256, 200), 20 * Config.LAYER_DELAY);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 200, 150), 20 * Config.LAYER_DELAY * 2);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 150, 100), 20 * Config.LAYER_DELAY * 3);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 100, 50), 20 * Config.LAYER_DELAY * 4);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 50, 0), 20 * Config.LAYER_DELAY * 5);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new deleteLayer(playerIsland, 0, 0), 20 * Config.LAYER_DELAY * 6);
    }

    private void deleteIsland(SimpleIsland playerIsland) {
        int x = playerIsland.getX() * Config.ISLE_SIZE;
        int z = playerIsland.getZ() * Config.ISLE_SIZE;

        for (int y_operate = 256; y_operate > 0; y_operate--) {
            for (int x_operate = x; x_operate < (x + Config.ISLE_SIZE); x_operate++) {
                for (int z_operate = z; z_operate < (z + Config.ISLE_SIZE); z_operate++) {
                    Block block = world.getBlockAt(x_operate, y_operate, z_operate);

                    if (block != null && !block.isEmpty()) {
                        block.getDrops().clear();

                        // Clear blocks with inventory
                        BlockState state = block.getState();
                        if (state instanceof InventoryHolder) {
                            InventoryHolder ih = (InventoryHolder) state;
                            ih.getInventory().clear();
                        }

                        deleteBlock(block);
                    }
                }
            }
        }

        // Clear entites before delete
        clearEntites(playerIsland);
    }

    private void setDefaultBiome(SimpleIsland isle) {
        int x = isle.getX() * Config.ISLE_SIZE;
        int z = isle.getZ() * Config.ISLE_SIZE;

        for (int x_operate = x; x_operate < (x + Config.ISLE_SIZE); x_operate++) {
            for (int z_operate = z; z_operate < (z + Config.ISLE_SIZE); z_operate++) {
                getIslandWorld().setBiome(x_operate, z_operate, DEFAULT_BIOME);
            }
        }
    }

    public void deleteBlock(Block block) {
        if(block.getType() != Material.AIR)
            block.setType(Material.AIR);
    }

    public void showChallengeList(CommandSender requestor, String plName, int from) {
        List<Challenge> challangeList = new ArrayList<>();

        challangeList.addAll(getChallenges().stream().filter(cha -> !playerCompletedChallenge(plName, cha.getId())).collect(Collectors.toList()));

        final int max = challangeList.size() / 10;
        final int start = from * 10;
        final int stop = start + 10;

        if (from > max)
            from = max;

        CorePlugin.sendHeader(requestor, "Zadania (" + from + "/" + max + ")"); //todo: zaczynajmy od strony 1, nie od zerowej

        int count = 0;
        for (Challenge chall : challangeList) {
            if (chall != null && count >= start) {
                CorePlugin.send(requestor, "&3#" + String.valueOf(chall.getId()) + " &7" + chall.getDescr());
            }
            count++;
            if (count >= stop)
                break;
        }

    }

    public boolean generateCacheLists() {
        boolean removeFreeList = false;

        Map<String, SimpleIsland> isList = new HashMap<>();
        isList.putAll(getIsleList());

        for (Entry<String, SimpleIsland> e : isList.entrySet()) {
            final String owner = e.getKey();
            final SimpleIsland island = e.getValue();

            if (island != null) {
                // Get coord hash
                final String coordHash = String.valueOf(island.getX() + "-" + island.getZ());
                // Put island
                if (coordList.containsKey(coordHash)) {
                    getIsleList().remove(owner);

                    removeFreeList = true;
                } else
                    coordList.put(coordHash, island);

                final List<String> members = new ArrayList<>();
                members.addAll(island.getMembers());

                if (!members.isEmpty()) {
                    for (String member : members) {
                        if (helpList.containsKey(member.toLowerCase())) {
                            island.removeMember(member);
                        } else
                            helpList.put(member.toLowerCase(), island);
                    }
                }

                // Generate uuid
                if (island.getOwnerUUID() != null)
                    uuidList.put(island.getOwnerUUID(), island);
            }
        }
        return removeFreeList;
    }

    public SimpleIsland getHelpingIsland(Player player) {
        return getHelpingIsland(player.getName());
    }

    public SimpleIsland getHelpingIsland(String plName) {
        final String lName = plName.toLowerCase();

        if (helpList.containsKey(lName))
            return helpList.get(lName);

        return null;
    }

    public int expellPlayers(Player owner, SimpleIsland island) {
        int count = 0;
        // Check
        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (pl == owner)
                continue;
            if (pl.hasPermission("islandworld.bypass.expell"))
                continue;
            if (pl.getWorld() != getIslandWorld())
                continue;
            if (island.isMember(pl.getName()))
                continue;

            if (isInsideIslandCuboid(pl, island)) {
                CorePlugin.send(pl, "&7Zostales wyeksmitowany z wyspy.");
                plugin.teleToSpawn(pl);
                count++;
            }
        }
        return count;
    }

    public boolean isDigit(String text) {
        if (text == null || text.isEmpty())
            return false;

        boolean result = true;
        char[] chars = text.toCharArray();
        for (char aChar : chars) {
            if (!Character.isDigit(aChar)) {
                result = false;
                break;
            }
        }
        return result;
    }

    public FileConfiguration getMessages() {
        if (customConfig == null) {
            customConfigFile = new File(getDataFolder(), "messages_def.yml");
            customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
        }
        return customConfig;
    }

    public boolean loadMessages() {
        boolean msgLoaded = true;

        final String langFile = "messages_def.yml";
        final File defFile = new File(getDataFolder(), langFile);
        if (!defFile.exists()) {
            defFile.getParentFile().mkdirs();
            copy(getResource("messages_def.yml"), defFile);
        }
        customConfigFile = new File(getDataFolder(), langFile);
        YamlConfiguration ccustomConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        InputStream defConfigStream = plugin.getResource("messages_def.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            ccustomConfig.setDefaults(defConfig);
        }

        try {
            ccustomConfig.options().copyDefaults(true);
            ccustomConfig.save(customConfigFile);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
            msgLoaded = false;
        }

        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        return msgLoaded;
    }

    public void saveOurConfig(File file) {
        getConfig().options().copyDefaults(true);
        try {
            getConfig().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLoc(String string) {
        String msg = getMessages().getString(string, "NOLOC_" + string);
        msg = msg.replaceAll("&n", "\n");
        msg = ChatColor.translateAlternateColorCodes('&', msg);
        return ChatColor.WHITE + msg;
    }

    public void setIsHelping(String name, SimpleIsland island) {
        final String plName = name.toLowerCase();

        if (helpList.containsKey(plName))
            helpList.remove(plName);

        helpList.put(plName, island);
    }

    public void removeHelping(String name) {
        final String plName = name.toLowerCase();

        if (helpList.containsKey(plName))
            helpList.remove(plName);
    }

    public boolean isHelping(Player player) {
        return isHelping(player.getName());
    }

    public boolean isHelping(String name) {
        return helpList.containsKey(name.toLowerCase());
    }

    public void clearHelping(SimpleIsland island) {
        final List<String> members = island.getMembers();
        if (members != null && !members.isEmpty()) {
            members.stream().filter(member -> member != null).forEach(this::removeHelping);
        }
    }

    public void makePartyRequest(Player owner, Player visitor) {
        partyList.put(owner.getName().toLowerCase(), visitor.getName().toLowerCase());

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new RemovePartyRequest(owner, visitor), 20 * getConfig().getInt("request-time"));
    }

    public HashMap<String, String> getPartyList() {
        return partyList;
    }

    public HashMap<String, String> getVisitList() {
        return visitList;
    }

    public boolean isOnDeleteList(String name) {
        return deleteList.contains(name);
    }

    public void addToDeleteList(String name) {
        deleteList.add(name);

        CorePlugin.send(Bukkit.getOfflinePlayer(name).getPlayer(), "&7Jestes tego pewien? Wpisz komende ponownie by usunac wyspe.");

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new RemoveDeleteReq(name), 20 * getConfig().getInt("request-time"));
    }

    public void removeFromDeleteList(String name) {
        deleteList.remove(name);
    }

    public void purgeIslands(CommandSender player, int days, int much) {
        IslandWorld.purgeInProgress = true;
        IslandWorld.breakPurge = false;

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Purge(this, player, days, much));
    }

    private void makeDebugLog(String string) {
        // Get last login time
        final long lastLogin = System.currentTimeMillis();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final String dateAsString = simpleDateFormat.format(lastLogin);

        try {
            File logFile = new File(getDataFolder(), "work.log");
            FileWriter fw = new FileWriter(logFile.toString(), true);
            fw.write("[" + dateAsString + "] " + string + "\r\n");
            fw.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    public boolean isCorrectBlock(Block b) {
        return (b.getType() != Material.AIR && b.getType() != Material.LAVA && b.getType() != Material.STATIONARY_LAVA
                && b.getType() != Material.WATER && b.getType() != Material.STATIONARY_WATER);
    }

    public boolean isInsideIsland(Player player, SimpleIsland island) {
        return isInsideIsland(player.getLocation(), island);
    }

    public boolean isInsideIsland(Location loc, SimpleIsland island) {
        // Check World
        if (loc.getWorld() != getIslandWorld())
            return false;

        if (island != null) {
            final int rs = island.getRegionSpacing();

            final int px = loc.getBlockX();
            final int pz = loc.getBlockZ();

            // Make math
            int x1 = (island.getX() * Config.ISLE_SIZE) + rs;
            int z1 = (island.getZ() * Config.ISLE_SIZE) + rs;

            int x2 = ((island.getX() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;
            int z2 = ((island.getZ() * Config.ISLE_SIZE) + Config.ISLE_SIZE) - rs;

            return (px >= x1 && px < x2 && pz >= z1 && pz < z2);
        }

        return false;
    }

    public boolean isInsideIslandCuboid(Player player, SimpleIsland island) {
        return isInsideIslandCuboid(player.getLocation(), island);
    }

    public boolean isInsideIslandCuboid(Location loc, SimpleIsland island) {
        if (loc.getWorld() != getIslandWorld())
            return false;

        if (island != null) {
            final int px = loc.getBlockX();
            final int pz = loc.getBlockZ();
            int x1 = (island.getX() * Config.ISLE_SIZE);
            int z1 = (island.getZ() * Config.ISLE_SIZE);
            int x2 = ((island.getX() * Config.ISLE_SIZE) + Config.ISLE_SIZE);
            int z2 = ((island.getZ() * Config.ISLE_SIZE) + Config.ISLE_SIZE);

            return (px > x1 && px < x2 && pz > z1 && pz < z2);
        }

        return false;
    }

    public boolean isInsideOwnIsland(Player player) {
        return !(player.getLocation().getBlockX() < 0 || player.getLocation().getBlockZ() < 0) && canBuildOnLocation(player, player.getLocation(), false);

    }

    public boolean canBuildOnLocation(Player player, Location loc) {
        return canBuildOnLocation(player, loc, true);
    }

    public boolean canBuildOnLocation(Player player, Location loc, boolean checkMembers) {
        if (player.isOp()) {
            return true;
        }
        if (loc.getBlockX() < 0 && loc.getBlockX() > -50) {
            return false;
        }
        if (loc.getBlockZ() < 0 && loc.getBlockZ() > -50) {
            return false;
        }
        if (loc.getBlockX() < 0 || loc.getBlockZ() < 0) {
            return false;
        }

        final String plName = player.getName().toLowerCase();
        final String coordHash = String.valueOf(loc.getBlockX() / Config.ISLE_SIZE) + "-" + String.valueOf(loc.getBlockZ() / Config.ISLE_SIZE);

        if (coordList.containsKey(coordHash)) {
            final SimpleIsland is = coordList.get(coordHash);
            if (is != null && isInsideIsland(loc, is)){
                if (is.getOwner() != null && is.getOwner().equalsIgnoreCase(plName)) {
                    return true;
                }
                if (checkMembers && is.isMember(plName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void showInfo(CommandSender sender, String who, SimpleIsland isle) {
        final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final List<String> members = isle.getMembers();
        final int isSize = (Config.ISLE_SIZE - (2 * isle.getRegionSpacing()));

        CorePlugin.sendHeader(sender, "Wyspa gracza " + who);
        CorePlugin.send(sender, "&7Wlasciciel: &3" + isle.getOwner());
        if(members != null && !members.isEmpty()){
            CorePlugin.send(sender, "&7Czlonkowie: &3" + StringUtils.join(members.toArray(), "&7, &3"));
        }
        CorePlugin.send(sender, "&7Numer wyspy: &3[" + isle.getX() + "," + isle.getZ() + "]");
        CorePlugin.send(sender, "&7Centrum wyspy: &3" + getIslandCenter(isle).getX() + "&7x&3" + getIslandCenter(isle).getBlockZ());
        CorePlugin.send(sender, "&7Utworzona: &3" + date.format(isle.getCreateTime()));
        CorePlugin.send(sender, "&7Wielkosc: &3" + isSize + "&7x&3" + isSize);
    }

    //todo: zrobic to jakos ladniej
    public void showStats(CommandSender player) {
        CorePlugin.sendHeader(player, "Statystyki");
        CorePlugin.send(player, "&3Wolne wyspy: &7" + freeList.size());
        CorePlugin.send(player, "&3Zajete wyspy: &7" + isleList.size());
    }

    public void teleportPlayer(World world, Player player, MyLocation loc) {
        player.teleport(new Location(world, loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch()));
    }

    public void onCreate(SimpleIsland newIsland, Player player, String schematicName) {
        // Get coord hash
        final String coordHash = newIsland.getX() + "-" + newIsland.getZ();
        // Put island
        coordList.put(coordHash, newIsland);
        // Get correct name
        final String plName = player.getName().toLowerCase();
        // Set schematic
        newIsland.setSchematic(schematicName);
        // Set time
        newIsland.setCreateTime(System.currentTimeMillis());
        // Set owner
        newIsland.setOwner(player.getName());
        // UUID Shit
        newIsland.setOwnerUUID(player.getUniqueId());
        uuidList.put(player.getUniqueId(), newIsland);
        // Remove from free
        getFreeList().remove(newIsland);
        // Add to taken list
        getIsleList().put(plName, newIsland);
        // Schedule
        scheduleRebuild(newIsland, player, schematicName);
        // Set not locked
        newIsland.setLocked(false);
        // Inc limits
        incCreateLimit(player);
        lastCreatedIsland.put(player.getName().toLowerCase(), System.currentTimeMillis());

    }

    public void onDelete(SimpleIsland isle, String who) {
        // Get coord hash
        final String coordHash = isle.getX() + "-" + isle.getZ();
        // Put island
        coordList.remove(coordHash);
        // Clear from Helping
        clearHelping(isle);
        // Delete challanges
        deleteChallenges(isle.getOwner());
        // Remove from taken
        getIsleList().remove(who.toLowerCase());
        // UUID shit
        UUID uid = isle.getOwnerUUID();
        if (uid != null && uuidList.containsKey(uid))
            uuidList.remove(uid);
        // Schedule
        scheduleRebuild(isle, null, null);
    }

    public void saveOurResource(String resourcePath) {
        if (resourcePath == null || resourcePath.equals("")) {
            throw new IllegalArgumentException("ResourcePath cannot be null or empty");
        }

        resourcePath = resourcePath.replace('\\', '/');
        InputStream in = getResource(resourcePath);
        if (in == null) {
            throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found in " + getFile());
        }

        File outFile = new File(getDataFolder(), resourcePath);
        int lastIndex = resourcePath.lastIndexOf('/');
        File outDir = new File(getDataFolder(), resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

        if (!outDir.exists()) {
            outDir.mkdirs();
        }

        try {
            if (!outFile.exists()) {
                OutputStream out = new FileOutputStream(outFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.close();
                in.close();
            }
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
        }
    }

    public boolean isSafeToTeleport(MyLocation l) {
        Block b0 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY() + 1, l.getBlockZ());
        Block b1 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        Block b2 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY() - 1, l.getBlockZ());

        return b0.getType() == Material.AIR && b1.getType() == Material.AIR && b2.getType().isSolid() && b2.getType() != Material.CACTUS;

    }

    public boolean isSafeToTeleport(Location l) {
        Block b0 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY() + 1, l.getBlockZ());
        Block b1 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        Block b2 = getIslandWorld().getBlockAt(l.getBlockX(), l.getBlockY() - 1, l.getBlockZ());

        return b0.getType() == Material.AIR && b1.getType() == Material.AIR && b2.getType().isSolid() && b2.getType() != Material.CACTUS;

    }

    public int[] getCoords(String playername, boolean members) {
        SimpleIsland is = getPlayerIsland(playername);
        if (is == null && members)
            is = this.getHelpingIsland(playername);

        if (is != null) {
            return new int[]{is.getX(), is.getZ()};
        }
        return null;
    }

    public boolean isLoaded() {
        return LOADED;
    }

    public OfflinePlayer getOfflinePlayer(String plName) {
        return Bukkit.getOfflinePlayer(plName);
    }

    public void teleToSpawn(Player pl) {
        pl.teleport(CorePlugin.getSpawnLocation());
    }

    public void removeNearbyMobsOnIsland(MyLocation loc) {
        int px = loc.getBlockX();
        int py = loc.getBlockY();
        int pz = loc.getBlockZ();

        for (int x = -1; x <= 1; x++) {
            for (int z = -1; z <= 1; z++) {
                Chunk c = IslandWorld.getIslandWorld().getChunkAt(new Location(IslandWorld.getIslandWorld(), px + x * 16, py, pz + z * 16));
                for (Entity e : c.getEntities()) {
                    if (e != null && e instanceof Monster) {
                        e.remove();
                    }
                }
            }
        }
    }

    public void dynDebug(String string){
        makeDebugLog(string);
    }

    public class AutoSave implements Runnable {
        @Override
        public void run() {
            if (LOADED && !REGEN_IN_PROGRESS) {
                try {
                    saveDatFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class Rebuild implements Runnable {
        private SimpleIsland _isle;
        private Player _player;
        private String _schem;

        public Rebuild(SimpleIsland isle, Player player, String schem) {
            _isle = isle;
            _player = player;
            _schem = schem;
        }

        @Override
        public void run() {
            if (_schem != null) {
                deleteIsland(_isle);
                setDefaultBiome(_isle);
                getSchematicManager().pasteSchematic(_player, _isle, _schem);
            } else {
                deleteLayers(_isle);
            }
        }
    }

    public class Regenerate implements Runnable {
        @Override
        public void run() {
            // Make temporary list
            final List<String> tmpTakenList = new ArrayList<>();
            final List<String> tmpFreeList = new ArrayList<>();

            for (Entry<String, SimpleIsland> en : getIsleList().entrySet()) {
                final SimpleIsland island = en.getValue();
                tmpTakenList.add(hashMe(island.getX(), island.getZ()));
            }

            for (int x_operate = 0; x_operate < Config.MAX_COUNT; x_operate++) {
                for (int z_operate = 0; z_operate < Config.MAX_COUNT; z_operate++) {
                    final String hash = hashMe(x_operate, z_operate);

                    if (!tmpFreeList.contains(hash) && !tmpTakenList.contains(hash)) {
                        SimpleIsland temp = new SimpleIsland(x_operate, z_operate);
                        freeList.add(temp);
                        tmpFreeList.add(hash);
                    }
                }
            }
            // Clear temp list
            tmpTakenList.clear();
            tmpFreeList.clear();

            getLogger().info("FreeList regenerated, new count: " + freeList.size());
            REGEN_IN_PROGRESS = false;
            try {
                saveDatFiles();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class deleteLayer implements Runnable {
        private SimpleIsland island;
        private int start;
        private int stop;

        public deleteLayer(SimpleIsland is, int sta, int sto) {
            island = is;
            start = sta;
            stop = sto;
        }

        @Override
        public void run() {
            int x = island.getX() * Config.ISLE_SIZE;
            int z = island.getZ() * Config.ISLE_SIZE;

            for (int y_operate = start; y_operate > stop; y_operate--) {
                for (int x_operate = x; x_operate < (x + Config.ISLE_SIZE); x_operate++) {
                    for (int z_operate = z; z_operate < (z + Config.ISLE_SIZE); z_operate++) {
                        Block block = world.getBlockAt(x_operate, y_operate, z_operate);

                        if (block != null && !block.isEmpty()) {
                            if (block.getDrops() != null)
                                block.getDrops().clear();

                            // Clear blocks with inventory
                            final BlockState state = block.getState();
                            if (state != null && state instanceof InventoryHolder) {
                                final InventoryHolder ih = (InventoryHolder) state;
                                if (ih.getInventory() != null)
                                    ih.getInventory().clear();
                            }

                            deleteBlock(block);
                        }
                    }
                }
            }
            if (start == 0 && stop == 0) {
                // Add island to free list
                getFreeList().add(0, new SimpleIsland(island.getX(), island.getZ()));
                clearEntites(island);
            }
        }
    }

    public class RemovePartyRequest implements Runnable {
        private Player owner;
        private Player visitor;

        public RemovePartyRequest(Player o, Player v) {
            owner = o;
            visitor = v;
        }

        @Override
        public void run() {
            final HashMap<String, String> tmpMap = new HashMap<>();
            tmpMap.putAll(getPartyList());

            for (Entry<String, String> x : tmpMap.entrySet()) {
                final String ow = x.getKey();
                final String vi = x.getValue();

                if (ow != null && vi != null && (ow.equalsIgnoreCase(owner.getName()) || vi.equalsIgnoreCase(visitor.getName()))) {
                    plugin.showError(owner, getLoc("error-party-visitor-no-reply").replaceAll("%name%", visitor.getName())); //todo
                    plugin.showError(visitor, getLoc("error-party-owner-no-reply").replaceAll("%name%", owner.getName())); //todo

                    getPartyList().remove(ow);
                }
            }
        }
    }

    public class RemoveDeleteReq implements Runnable {
        private String name;

        public RemoveDeleteReq(String n) {
            name = n;
        }

        @Override
        public void run() {
            if (name != null) {
                if (deleteList.contains(name)) {
                    removeFromDeleteList(name);
                    CorePlugin.send(Bukkit.getPlayer(name), "&cCzas na potwierdzenie usuniecia wyspy minal.");
                }
            }
        }
    }

    public class Purge implements Runnable {
        private Plugin pp;
        private CommandSender op;
        private int days;
        private int much;

        public Purge(Plugin pl, CommandSender p, int d, int m) {
            pp = pl;
            op = p;
            days = d;
            much = m;
        }

        @Override
        public void run() {
            final HashMap<String, SimpleIsland> isList = new HashMap<>();
            final HashMap<String, SimpleIsland> removeList = new HashMap<>();

            isList.putAll(getIsleList());

            int counter = 0;
            for (Entry<String, SimpleIsland> e : isList.entrySet()) {
                final String playerName = e.getKey();
                final SimpleIsland island = e.getValue();

                final long lastLogin = island.getOwnerLoginTime();
                if (!island.isPurgeProtected() && lastLogin > 0 && lastLogin < (System.currentTimeMillis() - (days * 86400000L))) {
                    removeList.put(playerName, island);

                    counter++;

                    if (much > 0 && counter == much)
                        break;
                }
            }
            if (op != null) {
                if (counter > 0)
                    plugin.showOpMessage(op, counter + " islands scheduled to delete.");
                else
                    plugin.showOpMessage(op, "No islands found to purge.");
            }

            // Schedule
            Bukkit.getScheduler().scheduleSyncDelayedTask(pp, new Remove(pp, op, removeList), 20 * getConfig().getInt("purge-delay", 5));
        }
    }

    public class Remove implements Runnable {
        private Plugin pp;
        private CommandSender op;
        private HashMap<String, SimpleIsland> li = new HashMap<>();

        public Remove(Plugin pl, CommandSender p, HashMap<String, SimpleIsland> list) {
            pp = pl;
            op = p;
            li = list;
        }

        @Override
        public void run() {
            if (li != null && !li.isEmpty()) {
                String playerName = null;
                SimpleIsland island = null;

                for (Entry<String, SimpleIsland> e : li.entrySet()) {
                    playerName = e.getKey();
                    island = e.getValue();

                    break;
                }

                if (IslandWorld.breakPurge) {
                    purgeInProgress = false;
                    breakPurge = false;

                    makeDebugLog("Purge stoped by break command");
                    return;
                }

                if (playerName != null && island != null) {
                    // Remove first
                    li.remove(playerName);
                    // Delete island
                    onDelete(island, playerName);
                    // Get last login time
                    final long lastLogin = island.getOwnerLoginTime();
                    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    final String dateAsString = simpleDateFormat.format(lastLogin);

                    // All done
                    if (op != null)
                        plugin.showOpMessage(op, "[" + dateAsString + "] " + ChatColor.WHITE + playerName + ChatColor.GRAY + "'s island deleted");

                    // Schedule again
                    Bukkit.getScheduler().scheduleSyncDelayedTask(pp, new Remove(pp, op, li), 20 * getConfig().getInt("purge-delay", 5));
                } else {
                    purgeInProgress = false;

                    makeDebugLog("Purge ends, owner or island null");

                    if (op != null)
                        plugin.showOpMessage(op, "Error, owner name or island null, skipping");
                }
            } else {
                purgeInProgress = false;

                makeDebugLog("Purge ends");

                if (op != null)
                    plugin.showOpMessage(op, "Purge list is empty, purge ends");
            }
        }
    }
}
