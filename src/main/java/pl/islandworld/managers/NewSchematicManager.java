package pl.islandworld.managers;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.schematic.SchematicFormat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import pl.islandworld.Config;
import pl.islandworld.IslandWorld;
import pl.islandworld.entity.SimpleIsland;

import java.io.File;
import java.io.IOException;

/**
 * @author Gnacik
 */
public class NewSchematicManager extends SchematicManager {
    private static IslandWorld plugin;

    public NewSchematicManager(IslandWorld plug) {
        plugin = plug;
    }

    @SuppressWarnings("deprecation")
    public void pasteSchematic(World world, File file, Location origin) {
        Vector v = new Vector(origin.getBlockX(), origin.getBlockY(), origin.getBlockZ());
        SchematicFormat schematic = SchematicFormat.getFormat(file);
        EditSession es = new EditSession(new BukkitWorld(world), 999999999);
        CuboidClipboard cc;

        try {
            cc = schematic.load(file);
            cc.paste(es, v, true);
        } catch (IOException | com.sk89q.worldedit.data.DataException | MaxChangedBlocksException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public void pasteSchematic(Player player, SimpleIsland island, String sName) {
        final World world = IslandWorld.getIslandWorld();
        if (player == null || world == null)
            return;

        // Generate it
        int x = (island.getX() * Config.ISLE_SIZE) + (Config.ISLE_SIZE / 2);
        int y = Config.ISLE_HEIGHT - 1;
        int z = (island.getZ() * Config.ISLE_SIZE) + (Config.ISLE_SIZE / 2);

        File schemaFile = new File(plugin.getDataFolder() + "/schematics/" + sName + ".schematic");

        Location loc = new Location(world, x, y, z);

        pasteSchematic(world, schemaFile, loc);

        int xx = island.getX() * Config.ISLE_SIZE;
        int zz = island.getZ() * Config.ISLE_SIZE;

        boolean spawnBlock = false;

        for (int y_operate = 256; y_operate > 0; y_operate--) {
            for (int x_operate = xx; x_operate < (xx + Config.ISLE_SIZE); x_operate++) {
                for (int z_operate = zz; z_operate < (zz + Config.ISLE_SIZE); z_operate++) {
                    Block block = world.getBlockAt(x_operate, y_operate, z_operate);

                    if (block != null && block.getTypeId() == 120) {
                        block.setType(Material.AIR);
                        island.setLocation(block.getLocation());
                        spawnBlock = true;
                    }
                }
            }
        }

        if (spawnBlock) {
            plugin.teleportPlayer(IslandWorld.getIslandWorld(), player, island.getLocation());
        } else {
            for (int y_operate = 0; y_operate < 255; y_operate++) {
                for (int x_operate = xx; x_operate < (xx + Config.ISLE_SIZE); x_operate++) {
                    for (int z_operate = zz; z_operate < (zz + Config.ISLE_SIZE); z_operate++) {
                        Block b1 = world.getBlockAt(x_operate, y_operate, z_operate);
                        Block b2 = world.getBlockAt(x_operate, y_operate + 1, z_operate);
                        Block b3 = world.getBlockAt(x_operate, y_operate + 2, z_operate);

                        Material b1t = b1.getType();

                        if (b1t != Material.AIR && b1t != Material.STATIONARY_LAVA && b1t != Material.LAVA && b1t != Material.CACTUS
                                && b2.getType() == Material.AIR && b3.getType() == Material.AIR) {
                            island.setLocation(b2.getLocation());
                            break;
                        }
                    }
                }
            }
        }
    }
}