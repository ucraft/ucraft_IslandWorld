package pl.islandworld.entity;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Challenge {
    private int id;
    private String descr;
    private List<ItemStack> reqItems;
    private boolean reqTake = false;
    private List<ItemStack> rewItems;
    private double rewEco;
    private boolean rep = false;

    public Challenge(int _id, String _descr, List<ItemStack> _req, boolean _take, List<ItemStack> _rew, double _rewEco, boolean _rep) {
        id = _id;
        descr = _descr;
        reqItems = _req;
        reqTake = _take;
        rewItems = _rew;
        rewEco = _rewEco;
        rep = _rep;
    }

    public int getId() {
        return id;
    }

    public String getDescr() {
        return descr;
    }

    public List<ItemStack> getRequiredItems() {
        return reqItems;
    }

    public boolean takeRequiredItems() {
        return reqTake;
    }

    public List<ItemStack> getRewardItems() {
        return rewItems;
    }

    public boolean isRepeatable() {
        return rep;
    }

    public double getRewEco() {
        return rewEco;
    }

    public void setRewEco(double rewEco) {
        this.rewEco = rewEco;
    }
}
