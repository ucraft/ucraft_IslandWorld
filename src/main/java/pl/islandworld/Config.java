package pl.islandworld;

import java.util.List;

/**
 * @author Gnacik
 */
public class Config {
    public static int MAX_COUNT = 150;
    public static int ISLE_SIZE = 200;
    public static int ISLE_HEIGHT = 120;
    public static int REGION_SPACING = 3;
    public static int REGION_STEP = 0;
    private static int RELOAD_TIME = 30;
    public static int AUTO_PURGE = 0;
    public static int LAYER_DELAY = 5;
    public static int CREATE_LIMIT = 0;
    public static List<String> ALLOWED_ITEM_LIST;
    private static IslandWorld plugin;

    public Config(IslandWorld plug) {
        plugin = plug;
    }

    public void setupDefaults() {
        ALLOWED_ITEM_LIST = (List<String>) plugin.getConfig().getList("allowed-list");

        RELOAD_TIME = plugin.getConfig().getInt("reload-time", 30);
        if (RELOAD_TIME <= 1)
            RELOAD_TIME = 30;
    }
}